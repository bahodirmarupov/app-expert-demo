package uz.pdp.appexpertserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appexpertserver.entity.PayType;
import uz.pdp.appexpertserver.entity.Role;
import uz.pdp.appexpertserver.entity.enums.RoleName;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findAllByRoleName(RoleName roleName);
}
