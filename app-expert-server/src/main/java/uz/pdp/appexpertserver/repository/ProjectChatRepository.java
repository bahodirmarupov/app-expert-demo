package uz.pdp.appexpertserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appexpertserver.entity.Project;
import uz.pdp.appexpertserver.entity.ProjectChat;

import java.util.UUID;

public interface ProjectChatRepository extends JpaRepository<ProjectChat, UUID> {
}
