package uz.pdp.appexpertserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.appexpertserver.entity.PayType;
import uz.pdp.appexpertserver.entity.Project;

import java.util.UUID;

@RepositoryRestResource(path = "payType")
public interface PayTypeRepository extends JpaRepository<PayType, Integer> {
}
