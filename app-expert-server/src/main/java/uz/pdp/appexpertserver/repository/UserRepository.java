package uz.pdp.appexpertserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appexpertserver.entity.User;

import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    boolean existsByTin(String tin);
    boolean existsByPhoneNumber(String phoneNumber);
    boolean existsByEmailEqualsIgnoreCase(String email);
    boolean existsByTinOrPhoneNumberOrEmailEqualsIgnoreCase(String tin, String phoneNumber, String email);
}
