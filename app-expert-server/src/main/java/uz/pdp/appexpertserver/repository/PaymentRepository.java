package uz.pdp.appexpertserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appexpertserver.entity.Payment;

import java.util.UUID;

public interface PaymentRepository extends JpaRepository<Payment, UUID> {
}
