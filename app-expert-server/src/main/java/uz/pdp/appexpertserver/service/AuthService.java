package uz.pdp.appexpertserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.appexpertserver.entity.User;
import uz.pdp.appexpertserver.entity.enums.RoleName;
import uz.pdp.appexpertserver.payload.ApiResponse;
import uz.pdp.appexpertserver.payload.ReqUser;
import uz.pdp.appexpertserver.repository.RoleRepository;
import uz.pdp.appexpertserver.repository.UserRepository;

import java.util.Collections;

@Service
public class AuthService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RoleRepository roleRepository;

    public ApiResponse addUser(ReqUser reqUser) {
        if (!userRepository.existsByTin(reqUser.getTin())) {
            if (!userRepository.existsByPhoneNumber(reqUser.getPhoneNumber())) {
                if (!userRepository.existsByEmailEqualsIgnoreCase(reqUser.getEmail())) {
                    User user = new User(
                            reqUser.getFirstName(),
                            reqUser.getLastName(),
                            reqUser.getMiddleName(),
                            reqUser.getPhoneNumber(),
                            reqUser.getEmail(),
                            passwordEncoder.encode(reqUser.getPassword()),
                            reqUser.getTin(),
                            reqUser.getPersonType(),
                            Collections.singleton(roleRepository.findAllByRoleName(RoleName.USER_ROLE)),
                            reqUser.getDistrict(),
                            reqUser.getRegion()
                            );
                }
                return new ApiResponse(false,"This email already used");
            }
            return new ApiResponse(false,"This phone number already used");
        }
        return new ApiResponse(false,"This Stir already used");
    }
}
