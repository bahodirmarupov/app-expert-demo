package uz.pdp.appexpertserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.pdp.appexpertserver.entity.Project;
import uz.pdp.appexpertserver.entity.enums.ProjectType;
import uz.pdp.appexpertserver.payload.ApiResponse;
import uz.pdp.appexpertserver.payload.ReqProject;
import uz.pdp.appexpertserver.repository.*;

import java.util.*;

@Service
public class ProjectService {

    @Autowired
    ProjectRepository repository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AttachmentRepository attachmentRepository;


    public ApiResponse addProject(ReqProject reqProject) {

        Project project=new Project();

        if (reqProject.getProjectType().equals(ProjectType.RECONSTRUCTION)) {
            project.setPermissionOrganisation(attachmentRepository.findAllByIdIn(reqProject.getPermissionOrganisation()).orElseThrow(() -> new ResourceNotFoundException("Ruxsatlar boycha fayllar topilmadi")));
            project.setEngineeringAndSearching(attachmentRepository.findAllByIdIn(reqProject.getEngineeringAndSearching()).orElseThrow(() -> new ResourceNotFoundException("Muhandislik izlanishlar boycha fayllar topilmadi")));
            project.setArt(attachmentRepository.findAllByIdIn(reqProject.getArt()).orElseThrow(() -> new ResourceNotFoundException("ART boycha fayllar topilmadi")));
            project.setConfirmedDraft(attachmentRepository.findAllByIdIn(reqProject.getConfirmedDraft()).orElseThrow(() -> new ResourceNotFoundException("Eskiz boycha fayllar topilmadi")));
            project.setWorking(attachmentRepository.findAllByIdIn(reqProject.getWorking()).orElseThrow(() -> new ResourceNotFoundException("Ishchi loyiha boycha fayllar topilmadi")));
        }
        if (reqProject.getProjectType().equals(ProjectType.CURRENT_REPAIRING)){
            project.setDefectAct(attachmentRepository.findAllByIdIn(reqProject.getDefectAct()).orElseThrow(() -> new ResourceNotFoundException("Nuqsonlar boycha fayllar topilmadi")));
            project.setTaskProject(attachmentRepository.findAllByIdIn(reqProject.getTaskProject()).orElseThrow(() -> new ResourceNotFoundException("Lohihalash boycha fayllar topilmadi")));
            project.setWorking(attachmentRepository.findAllByIdIn(reqProject.getWorking()).orElseThrow(() -> new ResourceNotFoundException("Ishchi loyiha boycha fayllar topilmadi")));
        }

        project.setAppNumber(reqProject.getAppNumber());
        project.setPersonType(reqProject.getPersonType());
        project.setUser(userRepository.findById(reqProject.getUserId()).orElseThrow(() -> new ResourceNotFoundException("User not found")));
        project.setExpert(userRepository.findById(reqProject.getExpertId()).orElseThrow(() -> new ResourceNotFoundException("Expert not found")));
        project.setProjectStatus(reqProject.getProjectStatus());
        project.setProjectType(reqProject.getProjectType());
        project.setExpertizeType(reqProject.getExpertizeType());
        project.setName(reqProject.getName());
        project.setProjector(reqProject.getProjector());
        project.setProjectorTin(reqProject.getProjectorTin());
        project.setProjectorPhoneNumber(reqProject.getProjectorPhoneNumber());
        project.setPrice(reqProject.getPrice());
        repository.save(project);
        return new ApiResponse(true, "Added!");
    }

    public List<Project> getAll() {
        return repository.findAll();
    }

    public Project getProject(UUID id) {
        return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Project not found!"));
    }

    public ApiResponse deleteProject(UUID id) {
        if (repository.existsById(id)) {
            return new ApiResponse(true, "Project deleted!");
        }
        return new ApiResponse(false, "Project not found!");
    }

    public ApiResponse editProject(UUID id, ReqProject reqProject) {
        if (repository.existsById(id)) {

            return new ApiResponse(true, "Undated!");
        }
        return new ApiResponse(false, "Project not found!");
    }

}
