package uz.pdp.appexpertserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.appexpertserver.entity.Attachment;
import uz.pdp.appexpertserver.entity.AttachmentContent;
import uz.pdp.appexpertserver.repository.AttachmentContentRepository;
import uz.pdp.appexpertserver.repository.AttachmentRepository;

import java.io.IOException;
import java.util.*;

@Service
public class AttachmentService {

    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;


    public List<UUID> uploadFile(MultipartHttpServletRequest request) throws IOException {
        Iterator<String> fileNames = request.getFileNames();
        List<UUID> list=new ArrayList<>();
        while (fileNames.hasNext()){
            MultipartFile file=request.getFile(fileNames.next());
            assert file != null;             // if(!file==null){...}
            Attachment attachment=new Attachment(
                    file.getOriginalFilename(),
                    file.getContentType(),
                    file.getSize()
            );
            Attachment savedAttachment = attachmentRepository.save(attachment);

            AttachmentContent attachmentContent=new AttachmentContent(
                    file.getBytes(),
                    savedAttachment
            );
            attachmentContentRepository.save(attachmentContent);

            list.add(savedAttachment.getId());
        }
        return list;
    }


    public HttpEntity<?> downloadFile(UUID uuid) {
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(uuid);
        if (optionalAttachment.isPresent()){
            Attachment attachment = optionalAttachment.get();
            return ResponseEntity.ok().contentType(MediaType.valueOf(attachment.getContentType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION,"attachment\""+attachment.getName()+"\"")
                    .body(attachmentContentRepository.findByAttachment(attachment).getContent());
        }
        return null;
    }
}
