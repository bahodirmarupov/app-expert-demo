package uz.pdp.appexpertserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.pdp.appexpertserver.entity.Payment;
import uz.pdp.appexpertserver.payload.ApiResponse;
import uz.pdp.appexpertserver.payload.ReqPayment;
import uz.pdp.appexpertserver.repository.PayTypeRepository;
import uz.pdp.appexpertserver.repository.PaymentRepository;
import uz.pdp.appexpertserver.repository.ProjectRepository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository repository;
    @Autowired
    PayTypeRepository payTypeRepository;
    @Autowired
    ProjectRepository projectRepository;

    public ApiResponse addPayment(ReqPayment reqPayment) {

        Payment payment = new Payment(
                payTypeRepository.findById(reqPayment.getPayTypeId()).orElseThrow(() -> new ResourceNotFoundException("Pay type not found!")),
                projectRepository.findById(reqPayment.getProjectId()).orElseThrow(() -> new ResourceNotFoundException("Project not found!")),
                reqPayment.getAmount(),
                new Timestamp(new Date().getTime()));
        repository.save(payment);
        return new ApiResponse(true, "Added!");
    }

    public List<Payment> getAll() {
        return repository.findAll();
    }

    public Payment getPayment(UUID id) {
        return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Payment not found!"));
    }

    public ApiResponse deletePayment(UUID id) {
        if (repository.existsById(id)) {
            return new ApiResponse(true, "Payment deleted!");
        }
        return new ApiResponse(false, "Payment not found!");
    }

    public ApiResponse editPayment(UUID id, ReqPayment reqPayment) {
        if (repository.existsById(id)) {
            Payment payment = new Payment(
                    payTypeRepository.findById(reqPayment.getPayTypeId()).orElseThrow(() -> new ResourceNotFoundException("Pay type not found!")),
                    projectRepository.findById(reqPayment.getProjectId()).orElseThrow(() -> new ResourceNotFoundException("Project not found!")),
                    reqPayment.getAmount(),
                    new Timestamp(new Date().getTime()));
            repository.save(payment);
            return new ApiResponse(true, "Added!");
        }
        return new ApiResponse(false, "Payment not found!");
    }
}
