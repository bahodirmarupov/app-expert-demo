package uz.pdp.appexpertserver.entity.enums;

public enum ProjectStatus {
    NEW,
    DIRECTED,
    PAYMENT_PENDING,
    IN_PROGRESS,
    COMPLETED,
    CANCELLED,
    REJECTED
}
