package uz.pdp.appexpertserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.appexpertserver.entity.template.AbsNameEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data@AllArgsConstructor@NoArgsConstructor
@Entity
public class PayType extends AbsNameEntity {
    private String color;
}
