package uz.pdp.appexpertserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uz.pdp.appexpertserver.entity.enums.PersonType;
import uz.pdp.appexpertserver.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "users")
@Data@AllArgsConstructor@NoArgsConstructor
public class User extends AbsEntity implements UserDetails {
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    private String middleName;
    @Column(unique = true,nullable = false)
    private String phoneNumber;
    @Column(unique = true,nullable = false)
    private String email; 
    @Column(nullable = false)
    private String password;
    @Column(unique = true,length = 9,nullable = false)
    private String tin;     //Stir or Inn
    @Enumerated(EnumType.STRING)
    private PersonType personType;
    @OneToMany
    private Set<Role> roles;
    private String district;
    private String region;

    private boolean isNonExpired=true;
    private boolean isNonLocked=true;
    private boolean isCredentialsNonExpired=true;
    private boolean isEnabled;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    public User(String firstName,String lastName, String middleName,
                String phoneNumber, String email,String password,String tin,
                PersonType personType,Set<Role> roles,String district,String region) {
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.email = email;
        this.tin= tin;
        this.personType =personType;
        this.roles=roles;
        this.district=district;
        this.region=region;
    }
}
