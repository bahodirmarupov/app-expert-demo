package uz.pdp.appexpertserver.entity.enums;

public enum  PersonType {
    PERSONAL,
    JURIDICAL
}
