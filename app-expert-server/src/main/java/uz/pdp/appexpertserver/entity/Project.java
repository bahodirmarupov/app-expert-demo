package uz.pdp.appexpertserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.appexpertserver.entity.enums.ExpertizeType;
import uz.pdp.appexpertserver.entity.enums.PersonType;
import uz.pdp.appexpertserver.entity.enums.ProjectStatus;
import uz.pdp.appexpertserver.entity.enums.ProjectType;
import uz.pdp.appexpertserver.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data@AllArgsConstructor@NoArgsConstructor
@Entity
public class Project extends AbsEntity {

    @Column(updatable = false)
    private Integer appNumber;

    @Enumerated(EnumType.STRING)
    private PersonType personType;

    @ManyToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private ProjectStatus projectStatus;

    @Enumerated(EnumType.STRING)
    private ProjectType projectType;

    @Enumerated(EnumType.STRING)
    private ExpertizeType expertizeType;

    private String name;
    private String projector;
    private String projectorTin;
    private String projectorPhoneNumber;

    private boolean seenAdmin;
    private boolean seenExpert;
    private boolean seenClient=true;

    @ManyToOne
    private User expert;

    private double price;

    @OneToMany
    private List<Attachment> permissionOrganisation;
    @OneToMany
    private List<Attachment> engineeringAndSearching;
    @OneToMany
    private List<Attachment> art;
    @OneToMany
    private List<Attachment> confirmedDraft;
    @OneToMany
    private List<Attachment> working;
    @OneToMany
    private List<Attachment> defectAct;
    @OneToMany
    private List<Attachment> taskProject;
}
