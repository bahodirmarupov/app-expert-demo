package uz.pdp.appexpertserver.entity.template;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@Data@AllArgsConstructor@NoArgsConstructor
@MappedSuperclass
public abstract class AbsNameEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nameUz;
    private String nameRu;
    private String nameEn;
//    @CreationTimestamp
//    @Column(updatable = false)
//    private Timestamp createdAt;
//    @UpdateTimestamp
//    private Timestamp updatedAt;
//    @CreatedBy
//    private UUID createdBy;
//    @LastModifiedBy
//    private UUID updatedBy;
}
