package uz.pdp.appexpertserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.appexpertserver.service.AttachmentService;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/attach")
public class AttachmentController {
    @Autowired
    AttachmentService service;

    @PostMapping
    public HttpEntity<?> uploadFile(MultipartHttpServletRequest request) throws IOException {
        return ResponseEntity.ok(service.uploadFile(request));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> downloadFile(@PathVariable(name = "id") UUID uuid) {
        return service.downloadFile(uuid);
    }
}
