package uz.pdp.appexpertserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appexpertserver.payload.ApiResponse;
import uz.pdp.appexpertserver.payload.ReqUser;
import uz.pdp.appexpertserver.security.JwtProvider;
import uz.pdp.appexpertserver.service.AuthService;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthService service;
    @Autowired
    JwtProvider jwtProvider;

    @PostMapping("/register")
    public HttpEntity<?> addUser(@Valid @RequestBody ReqUser reqUser) {
        ApiResponse response = service.addUser(reqUser);

        return ResponseEntity.ok(reqUser);
    }

}
