package uz.pdp.appexpertserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appexpertserver.entity.Payment;
import uz.pdp.appexpertserver.payload.ApiResponse;
import uz.pdp.appexpertserver.payload.ReqPayment;
import uz.pdp.appexpertserver.service.PaymentService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/payment")
public class PaymentController {
    @Autowired
    private PaymentService service;

    @PostMapping
    public ResponseEntity<ApiResponse> addPayment(@RequestBody ReqPayment reqPayment){
        ApiResponse response=service.addPayment(reqPayment);
        return ResponseEntity.status(response.isSuccess()? HttpStatus.CREATED:HttpStatus.CONFLICT).body(response);
    }

    @GetMapping
    public List<Payment> getAll(){
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Payment getPayment(@PathVariable UUID id){
        return service.getPayment(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deletePayment(@PathVariable UUID id){
        ApiResponse response=service.deletePayment(id);
        return ResponseEntity.status(response.isSuccess()? HttpStatus.CREATED:HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse> editPayment(@PathVariable UUID id,@RequestBody ReqPayment reqPayment){
        ApiResponse response=service.editPayment(id,reqPayment);
        return ResponseEntity.status(response.isSuccess()? HttpStatus.CREATED:HttpStatus.CONFLICT).body(response);
    }
}
