package uz.pdp.appexpertserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appexpertserver.entity.Project;
import uz.pdp.appexpertserver.payload.ApiResponse;
import uz.pdp.appexpertserver.payload.ReqProject;
import uz.pdp.appexpertserver.service.ProjectService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private ProjectService service;

    @PostMapping
    public ResponseEntity<ApiResponse> addPayment(@RequestBody ReqProject reqProject){
        ApiResponse response=service.addProject(reqProject);
        return ResponseEntity.status(response.isSuccess()? HttpStatus.CREATED:HttpStatus.CONFLICT).body(response);
    }

    @GetMapping
    public List<Project> getAll(){
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Project getProject(@PathVariable UUID id){
        return service.getProject(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteProject(@PathVariable UUID id){
        ApiResponse response=service.deleteProject(id);
        return ResponseEntity.status(response.isSuccess()? HttpStatus.CREATED:HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse> editProject(@PathVariable UUID id,@RequestBody ReqProject reqProject){
        ApiResponse response=service.editProject(id,reqProject);
        return ResponseEntity.status(response.isSuccess()? HttpStatus.CREATED:HttpStatus.CONFLICT).body(response);
    }
}
