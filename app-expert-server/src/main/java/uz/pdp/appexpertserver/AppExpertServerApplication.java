package uz.pdp.appexpertserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppExpertServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppExpertServerApplication.class, args);
	}

}
