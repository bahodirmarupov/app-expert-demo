package uz.pdp.appexpertserver.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class ReqPayment {
    private Integer payTypeId;
    private UUID projectId;
    private double amount;
}
