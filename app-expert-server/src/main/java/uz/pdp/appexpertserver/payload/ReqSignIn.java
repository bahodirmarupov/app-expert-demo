package uz.pdp.appexpertserver.payload;

import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
public class ReqSignIn {
    private String username;
    @Pattern(regexp = "((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,20})", message = "Not allowed password")
    private String password;
}
