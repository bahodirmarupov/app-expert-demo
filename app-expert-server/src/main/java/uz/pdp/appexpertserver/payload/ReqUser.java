package uz.pdp.appexpertserver.payload;

import lombok.Data;
import uz.pdp.appexpertserver.entity.Role;
import uz.pdp.appexpertserver.entity.enums.PersonType;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Set;

@Data
public class ReqUser {

    @NotBlank
    @Pattern(regexp = "^[A-Z][a-z]{2,16}$",message = "Wrong entered name")
    private String firstName, lastName;
    private String middleName;
    @Pattern(regexp = "^[+][9][9][8][0-9]{9}$",message = "Wrong entered number")
    private String phoneNumber;
    @Email(message = "wrong entered email")
    private String email;
    @Pattern(regexp = "((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,20})",message = "Not allowed password")
    // 0-9,a-z,A-Z lar qatnashishi va o'lchami 6-20 oraliqda bo'lishi kerak
    private String password;
    @Pattern(regexp = "^[0-9]{9}$")
    private String tin;     //Stir or Inn

    private PersonType personType;

    private String district;
    private String region;

}
