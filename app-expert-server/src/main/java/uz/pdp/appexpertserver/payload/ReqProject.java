package uz.pdp.appexpertserver.payload;

import lombok.Data;
import uz.pdp.appexpertserver.entity.Attachment;
import uz.pdp.appexpertserver.entity.User;
import uz.pdp.appexpertserver.entity.enums.ExpertizeType;
import uz.pdp.appexpertserver.entity.enums.PersonType;
import uz.pdp.appexpertserver.entity.enums.ProjectStatus;
import uz.pdp.appexpertserver.entity.enums.ProjectType;

import javax.persistence.OneToMany;
import java.util.List;
import java.util.UUID;

@Data
public class ReqProject {
    private Integer appNumber;

    private PersonType personType;

    private UUID userId;

    private UUID expertId;

    private ProjectStatus projectStatus;

    private ProjectType projectType;

    private ExpertizeType expertizeType;

    private String name;

    private String projector;

    private String projectorTin;

    private String projectorPhoneNumber;

    private double price;

    private List<UUID> permissionOrganisation;

    private List<UUID> engineeringAndSearching;

    private List<UUID> art;

    private List<UUID> confirmedDraft;

    private List<UUID> working;

    private List<UUID> defectAct;

    private List<UUID> taskProject;

}
