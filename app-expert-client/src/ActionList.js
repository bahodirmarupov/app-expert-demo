import React, {Component} from 'react';
import './App.css'

class ActionList extends Component {

    changeNumber = (diff) => {
        this.setState({number: this.state.number + Number.parseInt(diff)})
    }

    state = {
        number: 0
    }

    render() {
        const incr = () => {
            this.setState({number: this.state.number + 1})
        }
        const decr = () => {
            this.setState({number: this.state.number - 1})
        }
        return (
            <div className={"container"}>
                <h3>Action list</h3>
                <h1>{this.state.number}</h1>
                <button onClick={() => this.changeNumber('1')}>+</button>
                <button onClick={() => this.changeNumber('-1')}>-</button>
            </div>
        );
    }
}

export default ActionList;