import React from 'react';
import './App.css';
import TodoList from "./test/TodoList";


function App() {
    return (
        <div>
            <TodoList/>
        </div>
    );
}

export default App;
