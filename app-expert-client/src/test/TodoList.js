import React, {Component} from 'react';
import Todo from "./Todo";
import "./../App.css"

class TodoList extends Component {

    state = {
        todos: [
            {id: 1, name: 'uxlash', complated: true},
            {id: 2, name: 'ishlash', complated: false}
        ],
        show: false
    }

    render() {

        const showChange = () => {
            this.setState({show: !this.state.show})
        };

        const saveTodo = () => {
            let todoName = document.getElementById('todoName').value;
            let obj = {id: this.state.todos.length + 1, name: todoName, complated: false};
            let array = this.state.todos;
            array.push(obj)
            this.setState({todos: array});
            showChange();
        };

        const changeChecked = (id) => {
            let array = this.state.todos;
            array.forEach(value => {
                if (value.id === id) value.complated = !value.complated;
            });
            this.setState({todos: array})
        };

        return (

            <div className={'container'}>
                <div>
                    <h3>All tasks: {this.state.todos.length}</h3>
                    <h3>Complated tasks: {this.state.todos.filter(value => value.complated).length}</h3>
                    <h3>Notcomplated tasks: {this.state.todos.filter(value => !value.complated).length}</h3>
                </div>

                <h1 style={{textAlign: "center"}}>TodoList</h1>

                {!this.state.show ? <button onClick={showChange}>Add todo</button> : ''}
                {this.state.show ?
                    <div>
                        <input type="text" id={'todoName'}/>
                        <button onClick={showChange}>Cancel</button>
                        <button onClick={saveTodo}>Save</button>
                    </div> : ''}

                <p>
                    {this.state.todos.map((obj, index) =>
                        <Todo todo={obj} check={changeChecked}/>
                    )}
                </p>
            </div>
        );
    }
}

export default TodoList;