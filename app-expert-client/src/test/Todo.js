import React, {Component} from 'react';
import './style.css';

class Todo extends Component {
    render() {
        const {todo, check} = this.props;

        return (
            <div>
                <h4>{todo.id}</h4>
                <h4>{todo.name}</h4>
                <h4><input type="checkbox" defaultChecked={todo.complated} onChange={() => check(todo.id)}/></h4>
            </div>
        );
    }
}

export default Todo;